# Manga-List

## Description

Ce projet inclut:

* une application Android (créée avec App Inventor)
* un Web Service (développé avec Python)

Ce Web Service stocke les mangas envoyés par les utilisateurs via leurs smartphones, et récupère les informations éditoriales sur le Web.

## API du WebService 

Le webService contient 3 pages:

* [Page principale](https://manga-list-250207.appspot.com/): Liste des mangas de la base.
* [Store](https://manga-list-250207.appspot.com/storeavalue): Ajout d'éléments à la base.
* [Get](https://manga-list-250207.appspot.com//getvalue): lecture des information de la base.

## Store a Value

L'application Android envoie: **tag** et **value**.

* **tag** est le titre du manga. 
* **value** est le format du manga (`livre` ou `video`).

Ces informations sont stockées dans la base de données.

## Get Value

L'application Android envoie une commande (**tag**) et reçoit une liste d'informations en réponse (**value**).

Les commandes supportées sont:

* `titres:*`     retourne la liste de tous les titres de mangas de la base de données (max 200).
* `infos:AKIRA`  retourne toutes les informations liées à ce titre.
* `resume:AKIRA` retourne le résumé lié à ce titre.
* `pict:AKIRA`	 retourne le chemin de l'image liée à ce titre.



## Notes

[Apprendre Markdown](https://bitbucket.org/tutorials/markdowndemo)